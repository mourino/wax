/*
 * Usage:
 *   ./wax [options] <dir> -- <command>
 *   ./wax -ie js,json -ie py -id src -xd node_modules ./app -- node ./node_modules/bin/tsc
 *
 * Options:
 *   -ie --include-ext  Include by extension
 *   -id --include-dir  Include by directory name
 *   -xe --exclude-ext  Exclude by extension
 *   -xd --exclude-dir  Exclude by directory name
 */
#include <linux/inotify.h>

int main(int argc, char* argv[]) {
}
